/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MoneyInputListener implements ActionListener {
	private static String paymentStrategy = "";
	private TransactionController tc;
	
	public static String getPaymentStrategy() {
		return paymentStrategy;
	}

	public static void setPaymentStrategy(String paymentStrategy) {
		MoneyInputListener.paymentStrategy = paymentStrategy;
	}

	MoneyReceiver moneyReceiver;

	public MoneyReceiver getMoneyReceiver() {
		return moneyReceiver;
	}

	public void setMoneyReceiver(MoneyReceiver moneyReceiver) {
		this.moneyReceiver = moneyReceiver;
	}

	public MoneyInputListener(MoneyReceiver moneyReceiver) {
		this.moneyReceiver = moneyReceiver;
	}
	
	public MoneyInputListener(MoneyReceiver moneyReceiver, TransactionController tc) {
		this.tc = tc;
		this.moneyReceiver = moneyReceiver;
	}

	public void actionPerformed(ActionEvent ev) {
		Object obj = ev.getSource();
		if(obj instanceof CoinButton){
			CoinButton coinButton = (CoinButton)obj;  
			setMoneyReceiver(CoinReceiver.getInstance(tc));
			tc.setMoneyReceiver(getMoneyReceiver());
			moneyReceiver.receiveMoney(coinButton.getWeight());			
			setPaymentStrategy("COIN");
		}else{
			/*PaperMoneyButton paperMoneyButton = (PaperMoneyButton)obj;  
			moneyReceiver.receiveMoney(paperMoneyButton.getWeight());
			*/
			setPaymentStrategy("PAPERMONEY");
		}
	}
}