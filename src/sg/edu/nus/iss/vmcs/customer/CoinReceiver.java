/*
` * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

import java.util.ArrayList;

import sg.edu.nus.iss.vmcs.machinery.MachineryController;
import sg.edu.nus.iss.vmcs.store.CoinStore;
import sg.edu.nus.iss.vmcs.store.Coin;
import sg.edu.nus.iss.vmcs.store.Money;
import sg.edu.nus.iss.vmcs.store.Store;
import sg.edu.nus.iss.vmcs.util.VMCSException;

public class CoinReceiver extends MoneyReceiver {
	private static CoinReceiver instance = null;
	private CoinReceiver(TransactionController txCtrl) {
		super(txCtrl);
	}
	
	public static CoinReceiver getInstance(TransactionController txCtrl){
		if(instance == null){
			instance = new CoinReceiver(txCtrl);
		}
		return instance;
	}

	public void startReceiver(){
		txCtrl.getCustomerPanel().setCoinInputBoxActive(true);
		txCtrl.getCustomerPanel().setTotalMoneyInserted(0);
	}
	
	public void receiveMoney(double weight){
		CoinStore coinStore=(CoinStore)txCtrl.getMainController().getStoreController().getStore(Store.COIN);
		Coin coin=coinStore.findMoney(weight);
		if(coin==null){
			txCtrl.getCustomerPanel().displayInvalidCoin(true);
			txCtrl.getCustomerPanel().setChange("Invalid Coin");
		}
		else{
			txCtrl.getCustomerPanel().setCoinInputBoxActive(false);
			int value=coin.getValue();
			txCtrl.getCustomerPanel().displayInvalidCoin(false);
			arlMoney.add(coin);
			setTotalInserted(getTotalInserted() + value);
			txCtrl.getCustomerPanel().setTotalMoneyInserted(getTotalInserted());
			txCtrl.getCustomerPanel().setChange("");
			txCtrl.processMoneyReceived(getTotalInserted());
		}
	}

	public void continueReceive(){
		txCtrl.getCustomerPanel().setCoinInputBoxActive(true);
	}

	public boolean storeMoney(){
		MachineryController machineryCtrl=txCtrl.getMainController().getMachineryController();
		try{
			for(int i=0;i<arlMoney.size();i++){
				Coin coin=(Coin)arlMoney.get(i);
				machineryCtrl.storeCoin(coin);
			}
			resetReceived();
			txCtrl.getCustomerPanel().setTotalMoneyInserted(0);
		}
		catch(VMCSException ex){
			txCtrl.terminateFault();
			return false;
		}
		return true;
	}
	
	public void stopReceive(){
		CustomerPanel custPanel=txCtrl.getCustomerPanel();
		if(custPanel==null){
			return;
		}
		custPanel.setCoinInputBoxActive(false);
	}
	
	public void refundMoney(){
		if(getTotalInserted()==0)
			return;
		txCtrl.getCustomerPanel().setChange(getTotalInserted());
		txCtrl.getCustomerPanel().setTotalMoneyInserted(0);
		txCtrl.getCustomerPanel().displayInvalidCoin(false);
		resetReceived();
	}
	
	public void resetReceived(){
		arlMoney=new ArrayList<Money>();
		setTotalInserted(0);
	}
	
	public void setActive(boolean active){
		txCtrl.getCustomerPanel().setCoinInputBoxActive(active); 
	}
}