/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.customer;

import java.util.ArrayList;

import sg.edu.nus.iss.vmcs.store.Money;

public abstract class MoneyReceiver {
	
	private String paymentStrategy;
	
	public String getPaymentStrategy() {
		return this.paymentStrategy;
	}

	public void setPaymentStrategy(String paymentStrategy) {
		this.paymentStrategy = paymentStrategy;
	}

	protected TransactionController txCtrl;
	protected ArrayList<Money> arlMoney;
	protected int totalInserted;
	
	public MoneyReceiver(TransactionController txCtrl){
		this.txCtrl=txCtrl;
		arlMoney=new ArrayList<Money>();
		setTotalInserted(0);
	}
	
	public abstract void startReceiver();
	public abstract void stopReceive();
	public abstract void receiveMoney(double validation);
	public abstract void continueReceive();
	public abstract boolean storeMoney();
	public abstract void refundMoney();
	public abstract void resetReceived();
	public abstract void setActive(boolean active);

	public int getTotalInserted() {
		return totalInserted;
	}

	public void setTotalInserted(int totalInserted) {
		this.totalInserted = totalInserted;
	}
}