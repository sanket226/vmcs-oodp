/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.store;

public class Coin extends Money{

	public Coin() {
		super();
	}

	public Coin(int value, double weight) {
		super(value, weight);
	}
	
}