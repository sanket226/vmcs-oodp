package sg.edu.nus.iss.vmcs.store;

public class PaperMoney extends Money{

	public PaperMoney() {
		super();
	}

	public PaperMoney(int value, double weight) {
		super(value, weight);
	}

}
