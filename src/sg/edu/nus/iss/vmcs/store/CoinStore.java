/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.store;

public class CoinStore extends MoneyStore {
	private static CoinStore instance = null;
	
	private CoinStore() {
		/**This is the constant for coin invalid weight.*/
		INVALID_MONEY_VALIDATION = 9999;
	}
	
	public static CoinStore getInstance() {
		if(instance == null) {
			instance = new CoinStore();
		}
		return instance;
	}
	
	public int findMoneyStoreIndex (Money c) {
		int size = getStoreSize();
		for (int i = 0; i < size; i++) {
			StoreItem item = (CoinStoreItem) getStoreItem(i);
			Coin current = (Coin) item.getContent();
			if (current.getWeight() == c.getWeight())
				return i;
		}
		return -1;
	}

	public boolean isValidValidation(double weight){
		int size = getStoreSize();
		for (int i = 0; i < size; i++) {
			StoreItem item = (CoinStoreItem) getStoreItem(i);
			Coin current = (Coin) item.getContent();
			if (current.getWeight() == weight)
				return true;
		}
		return false;
	}
	
	public Coin findMoney(double weight){
		int size = getStoreSize();
		for (int i = 0; i < size; i++) {
			StoreItem item = (CoinStoreItem) getStoreItem(i);
			Coin current = (Coin) item.getContent();
			if (current.getWeight() == weight)
				return current;
		}
		return null;
	}
}