/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.store;

public abstract class MoneyStore extends Store {
	public static int INVALID_MONEY_VALIDATION;
	public MoneyStore() {
	}
	
	public abstract int findMoneyStoreIndex (Money c);

	public abstract boolean isValidValidation(double validation);
	
	public abstract Money findMoney(double validation);
}