package sg.edu.nus.iss.vmcs.store;

public class Money extends StoreObject{
	int value;
    double weight;

	public Money() {
	}

	public Money(int value, double weight) {
		super();
		this.value = value;
		this.weight = weight;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	

}
