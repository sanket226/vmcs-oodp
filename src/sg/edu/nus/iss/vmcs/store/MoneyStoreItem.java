/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.store;


public class MoneyStoreItem extends StoreItem {

	public MoneyStoreItem(Money coin, int qty) {
		super((StoreObject) coin, qty);
	}
}