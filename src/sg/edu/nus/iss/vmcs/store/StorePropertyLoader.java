package sg.edu.nus.iss.vmcs.store;


public abstract class StorePropertyLoader {

	protected PropertyLoader propertyLoader;

	abstract public StoreItem getItem (int index);
	abstract public void setItem (int index, StoreItem item);

	public abstract void setMode(String mode);
	
	public PropertyLoader getPropertyLoaderImpl(){		
		return propertyLoader;
	}
	
	public void setPropertyLoaderImpl(PropertyLoader propertyLoader){		
		this.propertyLoader = propertyLoader;
	}
	
	public String getValue(String inputName){
		return propertyLoader.getValue(inputName);
	}
	
	public void setValue(String inputKey, String inputValue){
		propertyLoader.setValue(inputKey,inputValue);
	}
	
	/*
	public int setValue(String inputKey, String inputValue){
		propertyLoader.setValue(inputKey,inputValue);
	}
	*/
}
