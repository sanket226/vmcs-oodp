/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.system;

import sg.edu.nus.iss.vmcs.store.Coin;
import sg.edu.nus.iss.vmcs.store.CoinStoreItem;
import sg.edu.nus.iss.vmcs.store.Money;
import sg.edu.nus.iss.vmcs.store.MoneyStoreItem;
import sg.edu.nus.iss.vmcs.store.StoreItem;
import sg.edu.nus.iss.vmcs.store.StorePropertyLoader;


public class CoinPropertyLoader extends StorePropertyLoader {

	private final String fileName ="CoinPropertyFile.txt";
	private static String NAME_LABEL;
	private static String WEIGHT_LABEL;
	private static String VALUE_LABEL;
	private static String QUANTITY_LABEL;
	
	public CoinPropertyLoader() {
		super();
		NAME_LABEL     = "Name";
		WEIGHT_LABEL   = "Weight";
		VALUE_LABEL    = "Value";
		QUANTITY_LABEL = "Quantity";
	}

	@Override
	public StoreItem getItem (int index) {
		int idx = index + 1;
		Money coin = new Coin();
		String name = new String(NAME_LABEL + idx);
		String value = getValue(name);
		coin.setName(value);
		name = new String(WEIGHT_LABEL + idx);
		value = getValue(name);
		coin.setWeight(Double.parseDouble(value));
		name = new String(VALUE_LABEL + idx);
		value = getValue(name);
		coin.setValue(Integer.parseInt(value));
		name = new String(QUANTITY_LABEL + idx);
		value = getValue(name);
		int qty = Integer.parseInt(value);
		MoneyStoreItem item = new CoinStoreItem(coin, qty);
		return item;
	}

	@Override
	public void setItem(int index, StoreItem cashItem) {
		int idx = index + 1;
		CoinStoreItem item = (CoinStoreItem) cashItem;
		Coin cn = (Coin) item.getContent();
		String itn = new String(NAME_LABEL + idx);
		setValue(itn, cn.getName());
		itn = new String(WEIGHT_LABEL + idx);
		setValue(itn, String.valueOf(cn.getWeight()));
		itn = new String(VALUE_LABEL + idx);
		setValue(itn, String.valueOf(cn.getValue()));
		itn = new String(QUANTITY_LABEL + idx);
		setValue(itn, String.valueOf(item.getQuantity()));
	}
	
	@Override
	public void setMode(String mode) {//If all will be using same loading method
		switch (mode.toLowerCase()){
		case "file":
			setPropertyLoaderImpl(new FilePropertyLoaderImpl(this.fileName));
			break;
		case "db":
			setPropertyLoaderImpl(new DatabasePropertyLoaderImpl());
			break;
		}
	}	
}