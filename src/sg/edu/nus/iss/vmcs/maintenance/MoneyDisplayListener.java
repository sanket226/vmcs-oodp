/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.maintenance;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class MoneyDisplayListener implements ActionListener {
	protected MaintenanceController mctrl;

	public MoneyDisplayListener(MaintenanceController mc) {
		mctrl = mc;
	}
	
	public abstract void actionPerformed(ActionEvent e);
}