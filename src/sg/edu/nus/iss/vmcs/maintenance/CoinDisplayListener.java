/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.maintenance;

import java.awt.Button;
import java.awt.event.ActionEvent;

public class CoinDisplayListener extends MoneyDisplayListener {
	public CoinDisplayListener(MaintenanceController mc) {
		super(mc);
	}

	public void actionPerformed(ActionEvent e) {
		String cmd;
		int idx;
		Button btn;
		btn = (Button) e.getSource();
		cmd = btn.getActionCommand();
		idx = Integer.parseInt(cmd);
		mctrl.displayCoin(idx);
	}
}