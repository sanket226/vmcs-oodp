/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.maintenance;

import sg.edu.nus.iss.vmcs.util.VMCSException;

public class CoinDisplay extends MoneyDisplay {
	private static final long serialVersionUID = 8736714221046063389L;

	public CoinDisplay(MaintenanceController mctrl) {
		super(mctrl);
	}

	public void setActive(boolean st) {
		bi.setActive(st);
	}

	public void displayQty(int idx, int qty) throws VMCSException {
		curIdx = idx;
		bi.clear();
		bi.displayQty(idx, qty);
	}

	public int getCurIdx() {
		return curIdx;
	}
}