/*
 * Copyright 2003 ISS.
 * The contents contained in this document may not be reproduced in any
 * form or by any means, without the written permission of ISS, other
 * than for the purpose for which it has been supplied.
 *
 */
package sg.edu.nus.iss.vmcs.maintenance;

import java.awt.*;

import sg.edu.nus.iss.vmcs.store.*;
import sg.edu.nus.iss.vmcs.util.VMCSException;

public abstract class MoneyDisplay extends Panel {

	public final static String TITLE = "Quantity of Money Available";
	private static final long serialVersionUID = -9005012181606703900L;
	protected StoreController storeCtrl;
	protected MaintenanceController mCtrl;
	protected ButtonItemDisplay bi;
	protected int len;
	protected int curIdx;

	public MoneyDisplay(MaintenanceController mctrl) {
		mCtrl = mctrl;
		storeCtrl = mCtrl.getMainController().getStoreController();
		len = storeCtrl.getStoreSize(Store.COIN);
		StoreItem[] items = storeCtrl.getStoreItems(Store.COIN);
		bi = new ButtonItemDisplay(TITLE, items, len);
		bi.addListener(new CoinDisplayListener(mCtrl));
		bi.clear();
		this.add(bi);
	}

	public abstract void setActive(boolean st);

	public abstract void displayQty(int idx, int qty) throws VMCSException;

	public abstract int getCurIdx();
}